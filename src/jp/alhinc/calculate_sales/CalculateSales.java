package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String FILE_SERIAL_NUMBER = "売上ファイルが連番になっていません";
	private static final String DIGIT_OVER = "合計金額が10桁を超えました";
	private static final String BRANCH_INVALID_CODE = "の支店コードが不正です";
	private static final String COMMODITY_INVALID_CODE = "の商品コードが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数の設定確認（エラー処理３）
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", "支店定義ファイル")) {
			return;
		}

		//商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[0-9a-zA-Z]{8}$",
				"商品定義ファイル")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();

			//ファイルなのかの確認（エラー処理３）
			if (files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//ファイルの連番チェック（エラー処理2）
		Collections.sort(rcdFiles);

		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int formaer = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - formaer) != 1) {
				System.out.println(FILE_SERIAL_NUMBER);
				return;
			}
		}

		//lineList（支店コード、売上合計金額）の作成
		BufferedReader saleBr = null;

		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				String rcdLine;
				//読み込んだ行を保持するリスト(支店コード,商品コード,売上合計金額)
				List<String> lineList = new ArrayList<>();

				saleBr = new BufferedReader(new FileReader(rcdFiles.get(i)));
				while ((rcdLine = saleBr.readLine()) != null) {
					lineList.add(rcdLine);
				}

				//売上ファイルのフォーマットをチェック（エラー処理２）(１行目：支店コード、２行目：商品コード、３行目：売上合計金額)
				if (lineList.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FILE_INVALID_FORMAT);
					return;
				}

				//売上ファイルの支店コードが支店定義ファイルに該当しなかった場合（エラー処理２）
				if (!branchSales.containsKey(lineList.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCH_INVALID_CODE);
					return;
				}

				// 売上ファイルの商品コードが商品定義ファイルに該当しなかった場合
				if (!commoditySales.containsKey(lineList.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + COMMODITY_INVALID_CODE);
					return;
				}

				//売上金額が数字かどうかの確認（エラー処理３）
				if (!lineList.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(lineList.get(2));
				Long saleAmount = branchSales.get(lineList.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(lineList.get(1)) + fileSale;

				//売上金額合計１０桁超えたかのチェック（エラー処理２）
				if (saleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println(DIGIT_OVER);
					return;
				}

				branchSales.put(lineList.get(0), saleAmount);
				commoditySales.put(lineList.get(1), commoditySaleAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (saleBr != null) {
					try {
						// ファイルを閉じる
						saleBr.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		//支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//商品定義ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName,
			Map<String, String> commodityNames,
			Map<String, Long> commoditySales,
			String matshesCode,
			String definitionName) {

		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//ファイルの存在チェック（エラー処理１-1）
			if (!file.exists()) {
				System.out.println(definitionName + FILE_NOT_EXIST);
				return false;
			}

			//ファイルの読み込み
			br = new BufferedReader(new FileReader(file));

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {//
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//ファイルのフォーマットをチェック（エラー処理１-2）
				if ((items.length != 2) || (!items[0].matches(matshesCode))) {
					System.out.println(definitionName + FILE_INVALID_FORMAT);
					return false;
				}
				commodityNames.put(items[0], items[1]);
				commoditySales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> commodityNames,
			Map<String, Long> commoditySales) {

		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			//path=args[0],fileName=branch.out（writeFileで定義）
			bw = new BufferedWriter(new FileWriter(new File(path, fileName)));

			for (String key : commodityNames.keySet()) {
				bw.write(key + "," + commodityNames.get(key) + "," + commoditySales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
